import psutil

import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish

import sys
import time
import platform

def cpu_times() :
    data = {
        'cpu': {
            'percents': psutil.cpu_percent()
        }
    }
    return data

def memory():
    bucketVM = psutil.virtual_memory()
    bucketSwap = psutil.swap_memory()
    data = {
        'memory': {
            'virtual' : {
                'total': bucketVM[0],
                'available': bucketVM[1],
                'percent': bucketVM[2],
                'used': bucketVM[3],
                'free': bucketVM[4],
                'active': bucketVM[5],
                'inactive': bucketVM[6],
                'wired': bucketVM[7]
            },
            'swap': {
                'total': bucketSwap[0],
                'used': bucketSwap[1],
                'free': bucketSwap[2],
                'percent': bucketSwap[3],
                'sin': bucketSwap[4],
                'sout': bucketSwap[5],
            }
        }
    }

    return data

def disks():
    bucketDisk = psutil.disk_usage('/')
    bucketCounters = psutil.disk_io_counters(perdisk=False)
    data = {
        'disks': {
            'usage' : {
                'total': bucketDisk[0],
                'used': bucketDisk[1],
                'free': bucketDisk[2],
                'percent': bucketDisk[3]
            },
            'io': {
                'readCount': bucketCounters[0],
                'writeCount': bucketCounters[1],
                'readBytes': bucketCounters[2],
                'writeBytes': bucketCounters[3],
                'readTime': bucketCounters[4],
                'writeTime': bucketCounters[5],
            }
        }
    }

    return data

def network():
    bucket = psutil.net_io_counters(pernic=True).iteritems()
    data = {
        'network': {
        }
    }

    for key, value in bucket:
        data['network'][key] = {
            'bytesSent': str(value[0]),
            'bytesReceived': str(value[1]),
            'packetsSent': str(value[2]),
            'packetsReceived': str(value[3]),
            'errorInput': str(value[4]),
            'errorOutput': str(value[5]),
            'dropInput': str(value[6]),
            'dropOutput': str(value[7])
            }

    return data

def packager(data):
    return {
        'nodeName': platform.node(),
        'time': int(time.time()),
        'data': data
    }

def on_connect(client, data, flags, rc):
    client.subscribe("/commands")

def on_message(client, userdata, msg):
    if str(msg.payload) == 'poweroff':      # extendable here to use multiple commands
        print "Exiting by command"
        sys.exit('Exited by command')
    elif str(msg.payload) == 'testcode':
        print "Running code testing suite"
        client.publish("/test", str(packager("7700 - Going to Heaven")))
        print "7700 - Going to Heaven"
        client.publish("/test", str(packager("7600 - Technical glitch")))
        print "7600 - Technical glitch"
        client.publish("/test", str(packager("7500 - Taken alive")))
        print "7500 - Taken alive"

def main():
    mqttc = mqtt.Client()
    mqttc.on_connect = on_connect
    mqttc.on_message = on_message
    mqttc.connect("localhost", 7734)

    while mqttc.loop() == 0:
        publish.single('/memory', str(packager(memory())), hostname="localhost", port=7734)
        publish.single('/cpu_times', str(packager(cpu_times())), hostname="localhost", port=7734)
        publish.single('/disks', str(packager(disks())), hostname="localhost", port=7734)
        publish.single('/network', str(packager(network())), hostname="localhost", port=7734)
        time.sleep(0.1)
        pass

if __name__ == "__main__":
    main()