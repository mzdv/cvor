Kanali koji se koriste za komunikaciju
======================================

`/memory`

RAM memorija

`/disks`

Diskovi

`/cpu`

Procesorski podaci

Format poruke za slanje podataka
================================

```
{
    'nodeName': 'ghost',
    'time': '1450200032,
    'data': {
        // trazeni podaci idu ovde u obliku objekta
    }
}
```